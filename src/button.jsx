import React, { Component } from "react";

export default class Button extends Component {
  numberButtonPress = value => {
    console.log(this.props.value);
    this.props.setcode(value)
  };

  render() {
    return (
      <div
        className="numberButton"
        onClick={() => {
          this.numberButtonPress(this.props.value);
        }}
      >
        {this.props.value}
      </div>
    );
  }
}
