import React, { Component } from "react";
import Button from "./button";
import map from './clue.jpg'
import headerImage from "./header.png";
import denied from "./access_denied.mp3";
import granted from "./access_welcome.mp3";
import buttonsound from "./button.mp3";
import "./App.css";

let accessGranted = new Audio(granted);
let accessDenied = new Audio(denied);
let buttonbeep = new Audio(buttonsound);

export default class App extends Component {
  constructor() {
    super();
    this.state = { secretCode: "", showReward: false };
  }

  setcode = number => {
    buttonbeep.play()
    // let secretCode = this.state.secretCode
    this.setState({ secretCode: this.state.secretCode + number });
  };

  enterCode = () => {
    if (this.state.secretCode === "01122012") {
      buttonbeep.play()
      accessGranted.play();
      this.setState({ showReward: true });
    } else {
      buttonbeep.play()
      accessDenied.play();
      this.clearCode()
    }
  };

  clearCode = () => {
    buttonbeep.play()
    this.setState({ secretCode: "" });
  };

  backOneCode = () => {
    buttonbeep.play()
    let removedOne = this.state.secretCode.slice(0, -1);
    this.setState({ secretCode: removedOne });
  };

  render() {
    if(this.state.showReward === false) {
    return (
      <div className="App">
        <header className="App-header">
          <img src={headerImage} alt="header" width="70%" />
          <p>Enter the secret code to get the next clue!</p>
          <table>
            <tbody>
              <tr>
                <td>
                  <Button value={1} setcode={this.setcode} />
                </td>
                <td>
                  <Button value={2} setcode={this.setcode} />
                </td>
                <td>
                  <Button value={3} setcode={this.setcode} />
                </td>
              </tr>
              <tr>
                <td>
                  <Button value={4} setcode={this.setcode} />
                </td>
                <td>
                  <Button value={5} setcode={this.setcode} />
                </td>
                <td>
                  <Button value={6} setcode={this.setcode} />
                </td>
              </tr>
              <tr>
                <td>
                  <Button value={7} setcode={this.setcode} />
                </td>
                <td>
                  <Button value={8} setcode={this.setcode} />
                </td>
                <td>
                  <Button value={9} setcode={this.setcode} />
                </td>
              </tr>
              <tr>
                <td>
                  <div className="delButton" onClick={this.clearCode}>
                    clear
                  </div>
                </td>
                <td>
                  <Button value={0} setcode={this.setcode} />
                </td>
                <td>
                  <div className="delButton" onClick={this.backOneCode}>
                    back
                  </div>
                </td>
              </tr>
              <tr>
                <td colspan="3">
                  <div className="enterButton" onClick={this.enterCode}>
                    Enter
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
          Secret Code
          <h2>{this.state.secretCode}</h2>
        </header>
      </div>
    )} else {
      return (
        <img alt="map" className="reward" src={map}/>
      )
    };
  }
}
